@extends('layouts.user.user-master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{ URL::previous() }}" class="btn btn-simple" id="icon2"><i class="fa fa-arrow-circle-left"></i><span id="spanIcon" style="font-size: 20px;">  Kembali</span></a>
                </div>
                <div class="col-md-8 offset-md-1">
                    <h3 class="card-title">Halaman Edit Artikel </h3> 
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('notif'))
        <div class="sufee-alert alert with-close alert-info alert-dismissible fade show mt-2 ml-3" id="close">
            <strong>Notif: </strong>
            {!! session('notif') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
   @endif
  <div class="row mt-3">
    <div class="col-md-12">
        <form action="{{ route('articles.update',['id'=>$articles->id]) }}" method="post">
            {{ method_field('patch') }}
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" class="form-control" length="200" name="title" aria-describedby="title" placeholder="tulis judul artikel anda" value="{{ $articles -> title }}">
            </div>
            <div class="form-group ">
                <label for="article"><ARTICLE>Tulis Artikel Anda</ARTICLE></label>
                <textarea name="articles" class="form-control" id="summernote">{{ $articles-> articles }}</textarea>
            </div>
            <div class="tools pull-right" id="icon">
                <button type="submit" class="btn btn-outline-primary rounded-circle ">
                    <i class="fa fa-send" ></i>
                </button>
                <button type="button" class="btn btn-outline-danger rounded-circle " onclick="clearForm(this.form)">
                    <i class="fa fa-undo"></i>
                </button>
            </div>
        </form>
    </div>
</div>
</div>
<script>
    function clearForm(oForm) {

        var elements = oForm.elements; 

        oForm.reset();

        for(i=0; i<elements.length; i++) {

            field_type = elements[i].type.toLowerCase();


            switch(field_type) {

                case "text":                                           
                elements[i].value = ""; 
                ;
                break;

                default: 
                break;
            }
        }
    }

</script>
@endsection
