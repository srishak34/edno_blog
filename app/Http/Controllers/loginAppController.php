<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

class loginAppController extends Controller
{
    public function login(Request $req) {
    	if($req -> isMethod('post')){
        $data = $req->input();

        if (Auth::guard('web')->attempt(['email' => $data['email'], 'password'=> $data['password']])) {          
          //echo "Success"; die;
          return redirect('user/dashboard');
        } else {
          //echo "Failed"; die;
          return redirect()->back()->with('notif','Username or Password Salah');
        }

      }
      if (Auth::guard('web')->check()) {
        return redirect('user/dashboard');
      }
      return view('auth.userLogin');
    }
}
// elseif (Auth::guard('supplier')->attempt(['contact_email' => $data['email'], 'password'=> $data['password']])) {
//             return redirect('/supplier/dashboard');
//         }
