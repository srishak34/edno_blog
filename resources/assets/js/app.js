/**
* First we will load all of this project’s JavaScript dependencies which
* includes Vue and other libraries. It is a great starting point when
* building robust, powerful web applications using Vue and Laravel.
*/


import VueRouter from 'vue-router';
import Welcome from './components/welcome.vue';
import Article from './components/artikel.vue';
import About from './components/about.vue';
import Readmore from './components/readmore.vue';

window.Vue = require('vue');
window.Vue.use(VueRouter);

require('./bootstrap');

/**
* Next, we will create a fresh Vue application instance and attach it to
* the page. Then, you may begin adding components to this application
* or customize the JavaScript scaffolding to fit your unique needs.
*/
const routes = [
	{
		path: '/', 
		name: 'welcome-page', 
		components: {
			'welcome-page': Welcome, 
		}, 
	},
	{
		path: '/articles', 
		name: 'article-page', 
		component: Article,
	},
	{
		path: '/about',
		name: 'about-page',  
		component: About, 
	},
	{
		path: '/article/readmore/:id',
		name: 'readmore',
		component: Readmore
	}
]

const router = new VueRouter({
	routes
});

const app = new Vue({
    el: '#app',
    router,
});
 $(document).ready(function() {
 	$('#summernote').summernote({
 		placeholder: 'tulis artikel anda yang menarik disini',
        height: 300
 	});
 });