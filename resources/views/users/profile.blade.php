@extends('layouts.user.user-master')
@section('content')
<style type="text/css">
    .remove-show {
      display: initial;
  }
  .remove-hidden {
      display: none;
  }
</style>
<div class="container-fluid">

    <div class="row">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Profile</h4>
                    @if (Session::has('notif'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Error!</strong> {!! session('notif') !!}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if (Session::has('sukses'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('sukses') !!}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

        </div>
        <form action="/user/profile/edit" enctype="multipart/form-data" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $user->id }}">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" placeholder="Nama" value="{{ $user -> name }}" name="name">
                        </div>
                    </div>
                    <div class="col-md-6 pl-1">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control" placeholder="Email" value="{{ $user -> email }}" name="email">
                        </div>
                    </div>
                </div>
                        {{-- <div class="row">
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" placeholder="Company" value="Mike">
                                </div>
                            </div>
                            <div class="col-md-6 pl-1">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" placeholder="Last Name" value="Andrew">
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control" placeholder="Home Address" value="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09">
                                </div>
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <label>Password Sekarang Ini</label>
                                    <input type="password" class="form-control" placeholder="Password" name="oldPass">
                                </div>
                            </div>
                            <div class="col-md-4 px-1">
                                <div class="form-group">
                                    <label>Password Baru</label>
                                    <input type="password" class="form-control" placeholder="New Password" name="newPass">
                                </div>
                            </div>
                            <div class="col-md-4 pl-1">
                                <div class="form-group">
                                    <label>Konfirmasi Password Baru</label>
                                    <input type="password" class="form-control" placeholder="Konfirmasi Password Baru" name="confirmNewPass">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tentang Saya</label>
                                    <textarea rows="4" style="height: 100px; resize: none;" {{-- cols="80" --}}  class="form-control" placeholder="Tentang Saya" name="about">{{ $user->about }}</textarea>
                                </div>
                            </div>
                        </div>
                        <input type="file" style="display: none;" accept="image/*" id="imageedit" name="imageedit">
                        <input type="hidden" id="image_old" name="image_old" value="{{ Storage::url($user->image) }}">
                        <button type="submit" class="btn btn-info btn-fill pull-right">Update Profile</button>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-user">
                <div class="card-image">
                    <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="...">
                </div>
                <div class="card-body">
                    <div class="author">
                        {{-- <a href="#"> --}}
                            <img id="u_image" class="avatar border-gray" src="{{ Storage::url($user->image) }}" alt="...">
                            <br><br>
                            <h3 style="color: #2BC9CF;">{{ $user->name }}</h3>
                            <h5 style="color: #2BC9CF;">{{ $user->email }}</h5>
                        {{-- </a> --}}
                        {{-- <p class="description">
                            michael24
                        </p> --}}
                    </div>
                    <p class="description text-center">
                        "{{ $user -> about }}"
                    </p>
                </div>
                <hr>
                <div class="button-container mr-auto ml-auto">
                    {{-- <button href="#" class="btn btn-simple btn-link btn-icon">
                        <i class="fa fa-facebook-square"></i>
                    </button>
                    <button href="#" class="btn btn-simple btn-link btn-icon">
                        <i class="fa fa-twitter"></i>
                    </button>
                    <button href="#" class="btn btn-simple btn-link btn-icon">
                        <i class="fa fa-google-plus-square"></i>
                    </button> --}}
                    <button type="button" class="btn btn-simple btn-primary btn-fill mb-1" onclick="editImage()" >Ganti Gambar Profil</button>
                    <button  type="button" id="removeEdit" class="remove-hidden btn-simple btn btn-secondary btn-fill mb-1" onclick="removeImageEdit()" class="">Semula</button>
                    <input type="hidden" style="display: none" value="0" name="remove" id="remove">
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/profile.js') }}"></script>
@endsection