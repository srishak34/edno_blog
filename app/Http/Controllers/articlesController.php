<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\article;
use Auth;
use Session;
use DB;


class articlesController extends Controller
{
    public function index()
    {
    	$user_id = Auth::User()->id;

    	$articles  = article::where('user_id', $user_id)->orderBy('id','asc')->paginate(6);


    	return view('articles.data-article',compact('articles'));

    }
    public function create(){
    	return view('articles.create-articles');
    }
    public function store(Request $request){
        $this->validate($request,array(
            'title'=> 'required|unique:articles'
        ));
        $id = Auth::user()->id;

        $store = new article;
        $store -> user_id  = $id;
        $store -> title    = $request -> title;
        $store -> articles = $request -> articles;

        $store-> save();
        $title = str_replace(' ', '-', $store->title);

        $back = redirect('/user/readmore/'.$title);


        return $back;

    }
    // public function show($title){
    // 	$articles = article::where('title',$title)->get();

    // 	return view('articles.single-article',compact('articles'));

    // }
    public function readmore($title){
        $titles = str_replace('-', ' ', $title);

        $articles = article::where('title',$titles)->first();
        return view('articles.single-article',compact('articles'));
    }
    public function edit($id){
        $articles = article::find($id);
        return view('articles.update-article',compact('articles'));
    }
    public function update(Request $request,$id)
    {

        $user_id = Auth::User()->id;

        $update= article::find($id);

        $title = $request->input('title');
        $unique = article::where('title', $title)->first();
        if ($unique !== null) {
            if ($unique->id === $update->id) {
                $update -> title = $title;
            }
            else {
                Session::flash('notif', 'tulis judul artikel baru karena yang ini sudah digunakan');
                return redirect()->back();
            }
        } else {
            $update -> title = $title;
        }

        // $update-> title = $request -> title;
        $update -> articles = $request -> articles;
        $update ->save();
        $title = str_replace(' ', '-', $update->title);

        $back = redirect('/user/readmore/'.$title);


        return $back;

    }
    public function destroy($id){
        $article = article::find($id);
        $article->delete();

        $back = redirect()->route('articles.index');

        return $back;


    }
    public function vue(){
        $articles=article::all();
        return $articles;
    }
    public function vuewelcome(){
         $articles = article::orderBy("id", "DESC")->paginate(3);
        return response()->json($articles);
    }
    public function vuereadmore($id) {
        $title=str_replace('-', ' ', $id);
        $read = article::where('title',$title)->first();
        $user_id = $read -> user_id;
        $user = User::findOrFail($user_id);
        $userImage = str_replace('public', 'storage', $user->image);


        return response()->json([
            'read' => $read,
            'user' => $user,
            'image' => $userImage
        ], 200);
    }
}
