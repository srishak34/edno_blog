@extends('layouts.user.user-master')
<style>

</style>
@section('content')
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9" style="border-radius: 5px;">
				<div class="card">
					<div class="header bg-light pt-3" id="title">
						<div class="row">
							<div class="col-md-2">
								<a href="../articles" class="btn btn-simple" id="icon2"><i class="fa fa-arrow-circle-left"></i><span id="spanIcon" style="font-size: 20px;">  Kembali</span></a>
							</div>
							<div class="col-md-10 text-center pull-left">
								<h3 class="title-article ">{{ $articles ->title }}</h3>
							</div>	
						</div>
					</div>
					<div class="card-body text-center">
						{!!$articles-> articles !!}
					</div>
					<hr>
					<div class="footer mr-2 pr-2">
						<div class="row">
							<div class="col-md-4">
								<img src="{{ Storage::url(Auth::user()->image) }}" alt="..." class="avatar border-gray pull-left pl-2"  style="height: 
								50px;width: 25%;">
								<p class="small pull-left ml-1">{{ Auth::user()->name }} <br>{{ date('y-m-d',strtotime($articles->created_at)) }}</p>
							</div>
							<div class="col-sm-8" id="icon2">
								<div class="row pull-right pr-2">
									<a href="../articles/{{ $articles->id }}/edit"><button class="btn btn-outline-info rounded-circle mr-2 pull-right"><i class="fa fa-pencil "></i></button></a>
									<form id="hapusData" action="{{ route('articles.destroy',['id'=>$articles->id]) }}" method="post">
										<button type="submit" onclick="return confirm('are you sure?')" class="btn btn-outline-danger rounded-circle pull-right "><i class="fa fa-trash"></i></button>
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="delete">
									</form>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="card card-user">
					<div class="image">
						<img src="{{ Storage::url(Auth::user()->image) }}"/ style="height: 150px; width: 100%;">
					</div>
					<div class="content">
						<div class="author">
							<a href="#">
								<img class="avatar border-gray" src="{{ Storage::url(Auth::user()->image) }}" alt="..."/>
								<h4 class="title">{{ Auth::user()->name }}<br />
									
								</h4>
							</a>
						</div>
						<p class="description text-center">"{{ Auth::user()->about }}"
						</p>
					</div>
					{{-- <hr>
					<div class="text-center">
						<button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
						<button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
						<button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>
					</div> --}}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection