<div class="sidebar" data-image="{{ asset('img/sidebar-5.jpg') }}">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="/" class="simple-text">
                Creative Edno Blog
            </a>
        </div>
        <div class="logo">
            <a href="#" class="simple-text">
                <img src="{{url('../img/faces/blog.png')}}" class="rounded-circle" style="width: 200px; height: 150px;">
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item {{ Request::is('user/dashboard')? 'active':'' }}" >
                <a class="nav-link" href="/user/dashboard">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item {{ Request::is('user/articles')? 'active':'' }}">
                <a class="nav-link" href="/user/articles">
                    <i class="nc-icon nc-paper-2"></i>
                    <p>Articles</p>
                </a>
            </li>
                    {{-- <li>
                        <a class="nav-link" href="./user.html">
                            <i class="nc-icon nc-circle-09"></i>
                            <p>User Profile</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="./table.html">
                            <i class="nc-icon nc-notes"></i>
                            <p>Table List</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="./typography.html">
                            <i class="nc-icon nc-paper-2"></i>
                            <p>Typography</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="./icons.html">
                            <i class="nc-icon nc-atom"></i>
                            <p>Icons</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="./maps.html">
                            <i class="nc-icon nc-pin-3"></i>
                            <p>Maps</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="./notifications.html">
                            <i class="nc-icon nc-bell-55"></i>
                            <p>Notifications</p>
                        </a>
                    </li>
                    <li class="nav-item active active-pro">
                        <a class="nav-link active" href="upgrade.html">
                            <i class="nc-icon nc-alien-33"></i>
                            <p>Upgrade to PRO</p>
                        </a>
                    </li> --}}
                </ul>
            </div>
        </div>

