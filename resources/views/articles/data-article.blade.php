@extends('layouts.user.user-master')
@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 pull-right">
				<a href="{{ route('articles.create') }}"><button class="btn btn-outline-primary pull-right"><i class="fa fa-plus"></i>tulis artikel</button></a>

			</div>
		</div>
		<div class="row mt-4">
			@if(count($articles) > 0)
			@foreach($articles as $article)
			<div class="col-md-4">
				<div class="card" id="card">
					<div class="header text-center bg-light">
						<h3 class="title-article p-3">{{ $article -> title }}</h3>
					</div>
					<div class="card-body text-center mb-2" id="article" >
						{!!$article-> articles!!}
					</div>
					<a href=" readmore/{{ str_replace(' ', '-', $article->title), Request::session()->put('id', $article->id)}}" class="text-center mt-2"><i class="fa fa-arrow-circle-right"></i>read more...</a>
					<hr>
					<div class="footer">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-6">
										<img src="{{ Storage::url(Auth::user()->image) }}" alt="..." class="border-gray pull-left pl-2" style="height: 
										50px;width: 100%;">
									</div>
									<div class="col-md-6 pull-left">
										<p class="small ml-1 ">{{ Auth::user()->name }}
											<small style="opacity: 0.8;">{{ date('d-m-Y', strtotime($article -> created_at)) }}</small>
										</p>
									</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="row pull-right">
									<a href="/user/articles/{{ $article -> id }}/edit"><button class="btn btn-outline-info rounded-circle mr-2" ><i class="fa fa-pencil" style="font-size: 20px;"></i></button></a>
									<form id="hapusData" action="{{ route('articles.destroy',['id'=>$article->id]) }}" method="post">
										{{ csrf_field() }}
										<button type="submit" class="btn btn-outline-danger rounded-circle" onclick="return confirm('are you sure?')" data-id="{{ $article->id }}"><i class="fa fa-trash" style="font-size:20px; "></i></button>
										<input type="hidden" name="_method" value="delete">
									</form>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			@endforeach
			@else
			<div class="col-md-12 text-center">
				<h2>Klik tombol tulis artikel untuk menulis artikel baru</h2>
			</div>
			@endif
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="pull-right">{{ $articles->links() }}</ul>
			</div>
		</div>
	</div>
</div>

@endsection
