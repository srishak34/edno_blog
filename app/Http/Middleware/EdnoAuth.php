<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class EdnoAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $user = 'notUser')
    {
        if ($user === 'notUser') {            
            Session::flash('notif', 'Mohon Login Terlebih dahulu');
            return redirect('/loginPage');
        } else {
            
            return $next($request);
        }
        
    }
}
