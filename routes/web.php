<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Welcome
Route::get('/', function()
{
	return view('welcome');
});
Route::get('/articles', function()
{
	return view('welcome');
});

// Testing
Route::get('/test', function () {
    return view('auth.userRegister');
});
Route::get('/articlevue','articlesController@vue');
Route::get('/articlevuewelcome','articlesController@vuewelcome');
Route::get('/articlevueread/{id}','articlesController@vuereadmore');

// Login
Route::match(['get', 'post'], '/loginPage', 'loginAppController@login')->name('loginPage');

// Register
Route::match(['get', 'post'], '/registerPage', 'registerAppController@register')->name('registerPage');

// User Dashboard
Route::group(['prefix' => 'user', 'middleware' => ['web', 'ednoAuth:itsUser']], function() {
    Route::get('/dashboard', 'userAppController@dashboard');
    Route::resource('/articles','articlesController');
    Route::get('/readmore/{title}','articlesController@readmore');
    Route::get('/logout', 'userAppController@logout');
    Route::get('/{id}/profile', 'userAppController@profile');
    Route::post('/profile/edit', 'userAppController@profileEdit');

});

// Default Laravel
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
