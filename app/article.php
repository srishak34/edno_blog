<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class article extends Model
{
    protected $filable =['id','title','articles'];

    public function User()
    {
    	return $this->belongsTo(User::class);
    }
}
