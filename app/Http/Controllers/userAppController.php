<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Storage;
use File;
use Image;
use Auth;
use Hash;
use App\User;

class userAppController extends Controller
{
    public function dashboard(Request $req)
    {
        $req->session()->flash('welcome', 'Welcome!');
        return view('users.dashboard');
    }
    public function logout()
    {
        Session::flush();
        return redirect('/loginPage')->with('notif', 'Berhasil Keluar :)');
    }
    public function profile($id)
    {

        $user = User::findOrFail($id);
        
        if ($user-> about === '') {
            $user-> about = "Tentang anda masih tidak diketahui";
        }

        return view('users.profile', compact('user'));
    }
    public function profileEdit(Request $request)
    {
        $update = User::findOrFail($request->input('id'));

        $uploadedImage = $request->file('imageedit');

        if($uploadedImage !== NULL)
        {

            $old = str_replace('/storage', '', $request->image_old);
            $nama_file=Storage::url($old);
            if(File::exists('.'.$nama_file))
                {
                    File::delete('.'.$nama_file);
                }
                $path   = $this->uploadImage($request, $uploadedImage);
            }
            else{
                $path = str_replace('/storage/', 'public/', $request->image_old);
            }

            $update -> name = $request -> name ;
            $update -> image = $path;

            $email = $request->input('email');
            $unique = User::where('email', $email)->first();
            if ($unique !== null) {
                if ($email === strtolower($unique->email) ) {
                    if (Auth::user()->id === $unique->id) {
                        
                        $update -> email = $email;
                    } else {
                        
                        Session::flash('notif', 'Email yang Anda Masukkan telah Digunakan.');
                        return redirect()->back();
                    }                    
                } 
            } else {
                
                $update -> email = $email;
            }
            
            $update -> about = $request -> about ;
            
            if ($request->input('oldPass') !== NULL) {
                if (Hash::check($request->input('oldPass'), $update->password)) {
                    $newPass = $request->input('newPass');
                    $newPassConfirm = $request->input('confirmNewPass');
                    if ($newPass === $newPassConfirm) {
                        $password = $request->input('newPass');
                        if ($password !== NULL) {
                            $name = $request -> name;       
                            $hashed = Hash::make($password);
                            $update -> password = $hashed;
                            $update -> save();
                            Session::flush();
                            return redirect('/loginPage')->with('notif','Password ' . $name . ' Telah diperbarui. Silahkan Login Kembali.');
                        } else {
                            Session::flash('notif', 'Password Baru Kosong');
                            return redirect()->back();
                        }
                    } else {
                        Session::flash('notif', 'Konfirmasi Password Baru Tidak Sama.');
                        return redirect()->back();
                    } 
                } else {
                    Session::flash('notif', 'Password Lama Tidak Sama.'); 
                    return redirect()->back();
                } 
            }
            Session::flash('sukses', 'Profile Berhasil di Update.');
            $update -> save();

            return redirect()->back();
        }

        function uploadImage($request ,$image)
        {
            $ext = '.'.$image->getClientOriginalExtension();
            $nama_file = $request -> input('name').$ext;

            $img = Image::make($image->getRealPath());

            $img->stream();

            $uploaded = Storage::disk('local')->put('public/user'.'/'.$nama_file, $img, 'public');

            $path = '';
            if($uploaded){
                $path = 'public/user/'.$nama_file;
            }
            return $path;
        }
        public function store(Request $request)
    {
        if (Auth::guard('web')->check()) {
            $this -> validate($request, [
                'item_code' => 'required|unique:items'
            ]);
            
        }
        // $uploadedImage = $request->file('image');

        // $path = $this->uploadImage($request,$uploadedImage);\

        $uploadedImage = $request->file('image');
        if ($uploadedImage !== NULL) {
            $path = $this->uploadImage($request ,$uploadedImage);
        } else {
            $path = str_replace('/storage/', 'public/', $request -> input('defaultimage'));

        }
            // Deklarasi $request
        if (Auth::guard('web')->check()) {
            $id = 0;
        } else {
            $id = Auth::user()->id;
        }

        $item_code = $request -> item_code;
        if (Auth::guard('web')->check()) {
            $code = $item_code;
        } else {
            $auth = Auth::user()->id;
            $item = mt_rand(1,1000);            
            $code = "profile".$auth."CODE".$item;
        }

        // if ($request->description !== null) {
        //     $description = $request->description;
        // } else {
        //     $description = "Data Supplier".$id;
        // }

        // if (Auth::guard('web')->check()) {
        
        
        // } else {
        //     $items = Item::where([['supplier_id', 0], ['name', $request->name]])->firstOrFail();
        
        // }
        if (Auth::guard('supplier')->check()) {
            $selected = $request -> input('id');
            $update = Item::findOrFail($selected);
            $update -> selected = Auth::user()->id;
            $update -> save();

        }
        if (Auth::guard('web')->check()) {
            $items = new item;
            $items -> qty = $request -> qty ;
        } else {
            $id = Auth::user()->id;
            $supplier = Item::where([['supplier_id', $id],['name', $request->input('name')]])->first();
            
            if (count($supplier) >= 1) {

                $items = $supplier;
                $items -> qty = $request -> qty + $items->qty ;
            } else {

                $items = new item;
                $items -> qty = $request -> qty ;
            }
        }

        $items -> name = $request -> name ;
        $items -> image = $path;
        $items -> email = $request -> email;
        $items -> description = $request -> description ;        
      

        $items -> save();



        Session::flash('success', 'Item Has Been Successfully Added.');


        if (Auth::guard('web')->check()) {                
            $back = redirect()->route('dashboard');
        } else {
            $back = redirect('/user/profile');
        }
        return $back;
    }
    }
