<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Laravel</title>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap-4.1.2-dist/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('css/style-welcome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('css/style-artikel.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body>
	@yield('content')
	<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
</body>
</html>
