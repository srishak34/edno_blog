<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Laravel</title>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap-4.1.2-dist/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('css/style-welcome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('css/style-artikel.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>
<body>
	<div id="app">
		<nav class="col-md-12 card p-1 nav-menu-2">
			<div class="container">
				<div class="row">
					<img src="img/faces/blog.png" style="width: 65px; height: 65px;">
					<div class="ml-auto">
						<ul>
							<li class="list-inline-item"><router-link :to="{name: 'welcome-page'}"> Beranda</router-link></li>
							<li class="list-inline-item"><router-link :to="{name: 'article-page'}"> Artikel</router-link></li>
							<li class="list-inline-item"><router-link :to="{name: 'about-page'}"> Tentang</router-link></li>
							@if(Route::has('login'))
							@auth
							<a href="{{ url('user/articles') }}">Kembali</a>
							@endauth
							@endif
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<router-view name="welcome-page"></router-view>
		<router-view></router-view>
	</div>
	<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
