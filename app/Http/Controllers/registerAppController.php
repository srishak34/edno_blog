<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use Hash;
use Auth;

class registerAppController extends Controller
{
    public function register(Request $req)
    {
        $this -> validate($req, [
            'email' => 'unique:users'
        ]);

        if($req -> isMethod('post')) {
            $user = new User;
            $user -> name = $req -> input('name');
            $user -> email = $req -> input('email');
            $password = $req -> input('newPass');
            if ($password !== null) {
                $passwordConfirm = $req -> input('confirmPass');
                if ($password == $passwordConfirm) {
                    $password = $req -> input('newPass');
                    $hashed = Hash::make($password);
                } else {
                    Session::flash('notif', 'Password Baru Tidak Sama');
                    return redirect()->back();
                }
            } else {
                Session::flash('notif', 'Password Baru Kosong');
                return redirect()->back();
            }
            $user -> password = $hashed;
            $user -> about = "Hallo!!! Saya baru mendaftar, mohon bantuannya :) ...";
            $user -> image = "public/face-0.jpg";
            $user -> save();

            $this->guard()->login($user);
            return redirect('user/dashboard');
            
        } else {
            return view('auth.userRegister');
        }
    }

    protected function guard()
    {
        return Auth::guard();
    }
}
