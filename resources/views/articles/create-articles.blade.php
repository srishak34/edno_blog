@extends('layouts.user.user-master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <a href="../../user/articles"><button class="btn btn-simple" id="icon2"><i class="fa fa-arrow-circle-left"></i> <span id="spanIcon"> Kembali</span></button>
                    </a>
                </div>
                <div class="col-md-8 offset-md-1">  
                    <h3 class="card-title">Buat Artikel Baru Anda</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form  action="{{ route('articles.store') }}" method="post" aria-label="{{ _('articles') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" data-validation-length="min30" aria-describedby="title"  placeholder="Tulis judul artikel anda" required="required">
                    @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group " >
                    <label for="article">Tulis Artikel</label>
                    <textarea name="articles" class="form-control" id="summernote"></textarea>
                </div>
                <div class="tools pull-right" id="icon">
                    <button type="submit" class="btn btn-outline-primary rounded-circle ">
                        <i class="fa fa-send"></i>
                    </button>
                    <button type="reset" class="btn btn-outline-danger rounded-circle ">
                        <i class="fa fa-undo"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
