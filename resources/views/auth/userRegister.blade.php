<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Daftar</title>
    {{-- <meta name="description" content=""> --}}
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{-- <meta name="robots" content="all,follow"> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('css/userLogin.css') }}" id="theme-stylesheet">
    
    
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>Edno Blog Register</h1>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                  @if(Session::has('notif'))
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {!! session('notif') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  @endif
                  @if($errors->has('email'))
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Maaf, Email Sudah Digunakan.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  @endif
                  <form action="/registerPage" method="post" class="form-validate">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <input id="register-name" type="text" name="name" required data-msg="Mohon Masukkan Nama Anda" class="input-material">
                      <label for="register-name" class="label-material">Nama</label>
                    </div>
                    <div class="form-group">
                      <input id="register-email" type="email" name="email" required data-msg="Mohon Masukkan Email Anda" class="input-material">
                      <label for="register-email" class="label-material">Alamat Email</label>
                    </div>
                    <div class="form-group">
                      <input id="register-password" type="password" minlength="6" maxlength="12" name="newPass" required data-msg="Mohon Masukkan Password Anda (Minimal Password: 6)" class="input-material">
                      <label for="register-password" class="label-material">Password</label>
                    </div>
                    <div class="form-group">
                      <input id="register-confirm-password" type="password" name="confirmPass" required data-msg="Konfirmasi Password Anda" minlength="6" maxlength="12" class="input-material">
                      <label for="register-confirm-password" class="label-material">Konfirmasi Password</label>
                    </div>
                    {{-- <div class="form-group terms-conditions">
                      <input id="register-agree" name="registerAgree" type="checkbox" required value="1" data-msg="Your agreement is required" class="checkbox-template">
                      <label for="register-agree">Agree the terms and policy</label>
                    </div> --}}
                    <div class="form-group">
                      <button id="register" type="submit" class="btn btn-primary">Daftar</button>
                    </div>
                  </form><small>Sudah Punya Akun? </small><a href="/loginPage" class="signup">Masuk</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Desain Web oleh <a href="https://bootstrapious.com" class="external">Bootstrapious</a>
          <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
        </p>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/jquery.cookie.js') }}"></script>
    <!-- Main File-->
    <script src="{{ asset('js/userLogin.js') }}"></script>
  </body>
</html>