<footer class="footer">
    <div class="container">
        <nav>
            <ul class="footer-menu">
                <li>
                    <a href="/">
                        Home
                    </a>
                </li>
                <li>
                    <a href="/#/articles">
                        Artikel
                    </a>
                </li>
                <li>
                    <a href="/#/about">
                        Tentang
                    </a>
                </li>
            </ul>
            <p class="copyright text-center">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                <a href="http://www.creative-tim.com">Creative Tim</a>
            </p>
        </nav>
    </div>
</footer>