<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
	public function welcome(){
		return view('welcome');
	}
	public function index()
	{
		return Post::orderBy('id','DESC')->get();
	}
}
