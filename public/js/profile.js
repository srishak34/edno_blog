function editImage() {        
  $('#imageedit').click();
}
$('#imageedit').change(function () {
  var imgPath = $(this)[0].value;
  var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
  if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg") {
    var oFile = document.getElementById("imageedit").files[0];
             if (oFile.size > 1000000) { // 2 mb for bytes.            
              alert("Ukuran file harus di bawah 1 MB!");
              $('#u_image').attr('src', '' + $('#image_old').val());
              $('#remove').val(1);
              $('#imageedit').val("");
              $('#removeEdit').removeClass('remove-show');
              $('#removeEdit').addClass('remove-hidden');                          
            }
            readEditURL(this);;
          }
          else {
            alert("Silakan pilih file gambar (jpg, jpeg, png).");
            $('#u_image').attr('src', '' + $('#image_old').val());
            $('#remove').val(1);
            $('#imageedit').val("");
            $('#removeEdit').removeClass('remove-show');
            $('#removeEdit').addClass('remove-hidden');

          }

        });

function readEditURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.readAsDataURL(input.files[0]);
    reader.onload = function (e) {
      $('#u_image').attr('src', e.target.result);
      $('#remove').val(0);
      $('#removeEdit').removeClass('remove-hidden');
      $('#removeEdit').addClass('remove-show');
    }
  }
}
function removeImageEdit() {        
  $('#u_image').attr('src', '' + $('#image_old').val());
  $('#remove').val(1);
  $('#imageedit').val("");
  $('#removeEdit').removeClass('remove-show');
  $('#removeEdit').addClass('remove-hidden');
}